package edu.haw.ttvp2;

import de.uniba.wiai.lspi.chord.data.URL;
import de.uniba.wiai.lspi.chord.service.PropertiesLoader;
import de.uniba.wiai.lspi.chord.service.impl.ChordImpl;

public class TestBroadcast {

	
	public static void main(String[] args) {
		try {
			PropertiesLoader.loadPropertyFile();
			
			ChordImpl network = new ChordImpl();
			URL networkURL = new URL(URL.KNOWN_PROTOCOLS.get(URL.LOCAL_PROTOCOL) + "://network0/");

			// create network
			network.setCallback(new TTVP(false));
			network.create(networkURL);
			System.out.println("0: "+network.getID());
			
			// create peers
			URL peerURL1 = new URL(URL.KNOWN_PROTOCOLS.get(URL.LOCAL_PROTOCOL) + "://peer1/");
			ChordImpl node1 = new ChordImpl();
			node1.join(peerURL1, networkURL);
			System.out.println("1: "+node1.getID());
			
			URL peerURL2 = new URL(URL.KNOWN_PROTOCOLS.get(URL.LOCAL_PROTOCOL) + "://peer2/");
			ChordImpl node2 = new ChordImpl();
			node2.join(peerURL2, networkURL);
			System.out.println("2: "+node2.getID());
			
			URL peerURL3 = new URL(URL.KNOWN_PROTOCOLS.get(URL.LOCAL_PROTOCOL) + "://peer3/");
			ChordImpl node3 = new ChordImpl();
			node3.join(peerURL3, peerURL2);
			System.out.println("3: "+node3.getID());
			
//			Key keyObject = new Key("test");
//			Value valueObject = new Value("value");
//			node3.insert(keyObject, valueObject);
//			
//			Set<Serializable> sets = node1.retrieve(keyObject);
//			System.out.println(sets);
			
			System.out.println(network.getSortedFingerTable());
			System.out.println(node1.getSortedFingerTable());
			System.out.println(node2.getSortedFingerTable());
			System.out.println(node3.getSortedFingerTable());
			
			System.out.println("~~~");
			Thread.sleep(1000);

			// 3 < 2 < 0 < 1
			System.out.println(network.getSortedFingerTable());
			System.out.println(node1.getSortedFingerTable());
			System.out.println(node2.getSortedFingerTable());
			System.out.println(node3.getSortedFingerTable());
			
			node1.broadcast(node1.getID(), true);
			
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
	}
}
