package edu.haw.ttvp2;

import java.util.ArrayList;
import java.util.List;

import de.uniba.wiai.lspi.chord.data.URL;
import de.uniba.wiai.lspi.chord.service.PropertiesLoader;

public class TestGame {
	public static void main(String[] args) throws Exception {
		PropertiesLoader.loadPropertyFile();
		
		int intervals = 100;
		int ships = 11;
		int enemies = 18;
		
		TTVP t1 = new TTVP(true);
		URL u1 = new URL(URL.KNOWN_PROTOCOLS.get(URL.SOCKET_PROTOCOL) + "://localhost:4441/");
		t1.create(u1);
		
		List<TTVP> list = new ArrayList<TTVP>();
		int start = 42;
		for (int i = 0; i < enemies; i++) {
			TTVP t = new TTVP(false);
			URL u = new URL(URL.KNOWN_PROTOCOLS.get(URL.SOCKET_PROTOCOL) + "://localhost:44"+(start + i)+"/");
			t.join(u, u1);
			list.add(t);
		}
		
		Thread.sleep(10000);
		
		t1.initAfterNetwork(intervals, ships);		
		for (TTVP t : list)
			t.initAfterNetwork(intervals, ships);
	}
}
