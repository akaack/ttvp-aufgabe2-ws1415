package edu.haw.ttvp2;

import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.WindowConstants;

public class GUIFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	
	public static final String WINDOW_NAME = "TTVP 2 - Awesome GUI (>^.^)>";
	private final GUIPanel panel;
	
	public GUIFrame(TTVP gameObject) {
		super(WINDOW_NAME);
		this.panel = new GUIPanel(gameObject);
		initGUI();
	}
	
	private void initGUI() {
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Exception e) {
			// ignore
		}
		
		GroupLayout thisLayout = new GroupLayout((JComponent) getContentPane());
		getContentPane().setLayout(thisLayout);
		thisLayout.setVerticalGroup(thisLayout.createSequentialGroup()
				.addComponent(panel, GroupLayout.PREFERRED_SIZE, 1000, GroupLayout.PREFERRED_SIZE));
		thisLayout.setHorizontalGroup(thisLayout.createSequentialGroup()
				.addComponent(panel, GroupLayout.PREFERRED_SIZE, 1800, GroupLayout.PREFERRED_SIZE));
		
		this.pack();	
		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		this.setSize(1800, 1000);	
		this.setResizable(false);
	}
	
	public static void main(String[] args) {
		new GUIFrame(null).setVisible(true);
	}
}
