/**
 * Hochschule fuer Angewandte Wissenschaften
 * TTV-P2 
 * 2014
 * 
 * by Alexander Kaack, Chris Marquardt
 */
package edu.haw.ttvp2;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

import de.uniba.wiai.lspi.chord.com.Node;
import de.uniba.wiai.lspi.chord.data.ID;
import de.uniba.wiai.lspi.chord.data.URL;
import de.uniba.wiai.lspi.chord.service.NotifyCallback;
import de.uniba.wiai.lspi.chord.service.PropertiesLoader;
import de.uniba.wiai.lspi.chord.service.ServiceException;
import de.uniba.wiai.lspi.chord.service.impl.ChordImpl;

public class TTVP implements NotifyCallback {

	public static final BigInteger MAX_VALUE = new BigInteger("2").pow(160)
			.subtract(BigInteger.ONE);

	ChordImpl node = new ChordImpl();
	GUIFrame guiObject = null;
	int interval = 100;
	int ships = 10;
	int shipsAlive = ships;
	BigInteger startID;
	BigInteger nodeID;
	BigInteger step;
	ID lastTarget = null;
	int shots = 0;
	int shotsAtUs = 0;
	int shotsAtUsMulti = 0;
	boolean end = false;
	
	List<BigInteger> intervals = new ArrayList<BigInteger>();
	Map<BigInteger, Integer> hitList = new HashMap<BigInteger, Integer>();
	Map<BigInteger, Boolean> shipList = new HashMap<BigInteger, Boolean>();
	Map<ID, Enemy> enemyList = new HashMap<ID, Enemy>();

	private class BroadcastThread extends Thread {
		ID target;
		boolean hit;

		public BroadcastThread(ID target, boolean hit) {
			this.target = target;
			this.hit = hit;
			this.start();
		}

		public void run() {
			node.broadcast(target, hit);
		}
	}

	private class RetrieveThread extends Thread {
		ID target;

		public RetrieveThread(ID target) {
			this.target = target;
			this.start();
		}

		public void run() {
			node.retrieve(target);
		}
	}

	/**
	 * Constructor
	 */
	public TTVP(boolean createGUI) {
		node.setCallback(this);
		if (createGUI) guiObject = new GUIFrame(this);
	}

	/**
	 * Creates Intervals
	 * @param startID start ID
	 * @param step step to the next ID
	 * @param interval count of Interval
	 * @return List of interval startpoints
	 */
	private List<BigInteger> createIntervals(String startID, BigInteger step,
			int interval) {
		List<BigInteger> list = new ArrayList<BigInteger>();
		BigInteger startIDBigInt = new BigInteger(startID);
		for (int i = 0; i < interval; i++) {
			list.add(startIDBigInt);
			startIDBigInt = startIDBigInt.add(step);
		}
		return list;
	}

	/**
	 * Initialize the start sequence
	 * @param interval count of intervals
	 * @param ships count of ships
	 */
	public void initAfterNetwork(int interval, int ships) {
		this.interval = interval;
		this.ships = ships;
		this.shipsAlive = ships;
		this.startID = node.getPredecessorID().toBigInteger()
				.add(BigInteger.ONE); // +1?
		this.nodeID = node.getID().toBigInteger();

		// System.out.println(startID);
		System.out.println("node: " + node.getID() + " / " + nodeID);

		// create intervals
		BigInteger dist = nodeID.subtract(startID).abs();
		if (nodeID.compareTo(startID) < 0)
			dist = MAX_VALUE.subtract(dist);
		this.step = dist.divide(new BigInteger(Integer.toString(interval)));
		this.intervals = createIntervals(startID.toString(), this.step,
				this.interval);

		// init hit map
		for (BigInteger i : intervals)
			this.hitList.put(i, 0);
		
		// set our ships
		placeShips();

		// calculate the enemies
		this.enemyList = calcEnemies(this.nodeID, this.node, this.ships,
				this.interval);
		System.out.println("Count of enemies: " + enemyList.size());

		// calculate the intervals of the enemies
		calcEnemyIntervals(this.enemyList, this.nodeID, this.interval);

		// should we start shooting?
		if (checkFirst()) {
			// shooting!
			System.out.println("First! Shooting");
			shoot();
		}

		if (guiObject != null)
			guiObject.setVisible(true);
	}
	
	/**
	 * Place the ships in our intervals
	 */
	private void placeShips() {
		int pairs = this.ships / 2;
		int single = this.ships % 2;

		// create zones for pairs
		int step = this.interval / pairs;
		Random random = new Random();
		for (int i = 0; i < pairs; i++) {
			int start = step * i;
			int index = random.nextInt(step - 1);

			shipList.put(intervals.get(start + index), true);
			shipList.put(intervals.get(start + index + 1), true);
		}

		// add single
		if (single > 0) {
			int index = random.nextInt(this.interval);
			while (shipList.get(intervals.get(index)) != null) {
				index = random.nextInt(this.interval);
			}
			shipList.put(intervals.get(index), true);
		}

//		System.out.println(shipList.size());
//		System.out.println(shipList);
	}

	/**
	 * Calculate the enemies intervals
	 * @param enemyList list of all enemies
	 * @param nodeID our node id
	 * @param interval count of intervals
	 */
	private void calcEnemyIntervals(Map<ID, Enemy> enemyList,
			BigInteger nodeID, int interval) {
		List<BigInteger> enemies = new ArrayList<BigInteger>();
		for (ID i : enemyList.keySet())
			enemies.add(i.toBigInteger());
		enemies.add(nodeID);
		Collections.sort(enemies);

		for (int i = 0; i < enemies.size(); i++) {
			BigInteger pre = enemies.get((i - 1 < 0 ? enemies.size() - 1
					: i - 1));
			Enemy enemy = enemyList.get(ID.valueOf(enemies.get(i)));
			// System.out.println(e);

			if (enemy != null) {
				enemy.start = pre.add(BigInteger.ONE);
				// intervals
				BigInteger d = enemy.end.subtract(enemy.start).abs();
				if (enemy.end.compareTo(enemy.start) < 0)
					d = MAX_VALUE.subtract(d);
				BigInteger step = d.divide(new BigInteger(Integer
						.toString(interval)));
				enemy.step = step;

				List<BigInteger> intervals = createIntervals(
						enemy.start.toString(), step, interval);
				for (BigInteger s : intervals)
					enemy.hitMap.put(s, 0);
			}
		}

	}

	/**
	 * Calculate the enemies
	 * @param nodeID our node id
	 * @param node our node object
	 * @param ships the count of ships
	 * @param interval the count of intervals
	 * @return a map of id with a enemy object
	 */
	private Map<ID, Enemy> calcEnemies(BigInteger nodeID, ChordImpl node,
			int ships, int interval) {
		Map<ID, Enemy> enemyList = new HashMap<ID, Enemy>();
		ID next = ID.valueOf(nodeID.add(BigInteger.ONE));
		Node nextNode = node.findSuccessor(next);

		while (nextNode.getNodeID().compareTo(node.getID()) != 0) {
			Enemy enemy = new Enemy(nextNode.getNodeID(), nextNode.getNodeURL(), ships, interval);
			enemyList.put(nextNode.getNodeID(), enemy);

			next = ID.valueOf(nextNode.getNodeID().toBigInteger()
					.add(BigInteger.ONE));
			nextNode = node.findSuccessor(next);
		}
		return enemyList;
	}

	/**
	 * Check if the are first
	 * @return true or false
	 */
	private boolean checkFirst() {
		return testInterval(startID, nodeID, MAX_VALUE);
	}

	
	@Override
	public void retrieved(ID target) {
		System.err.println("retrieved "+target);
		shotsAtUs++;
		BigInteger tId = target.toBigInteger();
		
		// add to hitlist
		for (BigInteger inter : intervals) {
//			if (tId.compareTo(inter) >= 0
//					&& tId.compareTo(inter.add(step)) < 0) {	
			if (testInterval(inter, inter.add(step), tId)) {
				int hits = hitList.get(inter) + 1;
				hitList.put(inter, hits);
				if (hits > 1) shotsAtUsMulti++;
				break;
			}
		}
		
		// check the ship
		boolean hit = checkShip(tId, this.shipList, this.step);
		if (hit)
			this.shipsAlive--;

		// broadcast the result
		new BroadcastThread(target, hit);

		// we can shoot now
		if (shipsAlive > 0)
			shoot();
		
		if (guiObject != null)
			guiObject.repaint();
	}

	/**
	 * Check if a ship is destroyed
	 * @param target the target id as bigint
	 * @param shipList the list of all ships
	 * @param step the step to the next id
	 * @return returns if one ship are destroyed
	 */
	private boolean checkShip(BigInteger target,
			Map<BigInteger, Boolean> shipList, BigInteger step) {
		boolean returnValue = false;
		for (BigInteger ship : shipList.keySet()) {
//			if (target.compareTo(ship) >= 0
//					&& target.compareTo(ship.add(step)) < 0) {				
			if (testInterval(ship, ship.add(step), target)) {
				// destroyed??
				if (!shipList.get(ship))
					break;
				else {
					shipList.put(ship, false);
					returnValue = true;
					System.err.println(ID.valueOf(nodeID)
							+ " Ship lost! Shipcount: " + (shipsAlive - 1));
					break;
				}
			}
		}
		return returnValue;
	}

	/**
	 * The shoot operation
	 */
	private void shoot() {
		if (end) return;
		
		List<Enemy> enemies = new ArrayList<Enemy>();
		for (Enemy enemy : this.enemyList.values())
			enemies.add(enemy);
		Collections.sort(enemies);

		Enemy target = enemies.get(0);
		if (target == null) {
			System.err.println("no enemy!");
			return;
		}

		ID targetID = target.getNextTarget();
		if (targetID == null) {
			System.err.println("no enemy id!");
			return;
		}

		// wait for broadcasting
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
		}

		this.lastTarget = targetID;

		System.out.println(node.getID() + " shooting @ enemy " + target.id
				+ " at field " + targetID + " (" + (shots++) + ")");
		new RetrieveThread(targetID);
	}

	@Override
	public void broadcast(ID source, ID target, Boolean hit) {
		System.out.println("broadcast "+source+" @ "+target+" = "+hit);
		
		// get enemy
		Enemy enemy = enemyList.get(source);
		if (this.lastTarget != null)
			if (target.compareTo(this.lastTarget) == 0)
				if (hit) {
					System.out.println("We have a hit! "
							+ ID.valueOf(nodeID) + " @ " + enemy.id);
				} else {
					System.out.println("Damn it! No hit! "
							+ ID.valueOf(nodeID) + " @ " + enemy.id);
				}
		// hit us?
		if (!testInterval(startID, nodeID, target.toBigInteger())) {

			// register hit
			enemy.registerHit(target, hit);

			if (enemy.ships < 1) {
				System.out.println("Game Over! Enemy " + enemy.id + " has "
						+ enemy.ships + " ships!");

				if (target.compareTo(lastTarget) == 0) {
					System.out.println("Win! "
							+ ID.valueOf(nodeID));
				}

				end = true;
//				System.exit(0);
			}
		}
		
		if (guiObject != null)
			guiObject.repaint();
	}

	/**
	 * Test if the value is in the interval
	 * @param start start of the interval
	 * @param end of the interval
	 * @param value for testing
	 * @return the result of this search
	 */
	private boolean testInterval(BigInteger start, BigInteger end,
			BigInteger value) {
		ID s = ID.valueOf(start.subtract(BigInteger.ONE));
		ID e = ID.valueOf(end.add(BigInteger.ONE));
		ID v = ID.valueOf(value);
		return v.isInInterval(s, e);
	}

	public void create(URL own) throws ServiceException {
		this.node.create(own);
	}

	public void join(URL own, URL to) throws ServiceException {
		this.node.join(own, to);
	}

	public static void main(String[] args) throws Exception {
		PropertiesLoader.loadPropertyFile();

		System.out.println("TTVP2");
		TTVP program = new TTVP(true);
		URL us = new URL(URL.KNOWN_PROTOCOLS.get(URL.SOCKET_PROTOCOL)
				+ "://141.22.27.61:4444/");
		System.out.println(us);

		Scanner sc = new Scanner(System.in);
		// get URL
		System.out.print("Connect to: ");
		String c = sc.nextLine();

		if (c.startsWith("s"))
			program.node.create(us);
		else {
//			URL url = new URL(c.trim());
			URL url = new URL(URL.KNOWN_PROTOCOLS.get(URL.SOCKET_PROTOCOL)
					+ "://141.22.27.63:2346/"); //141.22.27.63:8086
			System.out.println("Connecting... " + url);
			program.node.join(us, url);
		}

		// intervals
		System.out.print("Intervals: ");
		int i = sc.nextInt();
		System.out.print("Ships: ");
		int s = sc.nextInt();

		System.out.println(program.node.getFingerTable());
		
		// waiting
		System.out.println("Waiting...");
		sc.nextLine();
		sc.nextLine();

		// init
		System.out.println("Init!");
		program.initAfterNetwork(i, s);

		sc.close();
	}

	/**
	 * Generelle Strategie:
	 * 
	 * Plazierung der Schiffe:
	 * Die Schiffe werden in zwei hiereinander gefolgten intervallen zufaellig plaziert.
	 * 
	 * Schussstrategie:
	 * Finde alle Gegner und erechne die Intervalle der Gegner um nur in die Intervalle zu schiessen.
	 * Jeder Schuss der im Netz bekannt gegeben wird, wird in einer Liste vermerkt, 
	 * sodass nicht in bereits beschossene Intervalle erneut geschossen wird.
	 * 
	 * Es wird eine Wahrscheinlichkeit fuer Treffer je Gegner errechnet und der Gegner mit 
	 * der hoechsten Trefferwahrscheinlichkeit ausgewählt.
	 * 
	 * Das Zielinterval des Gegners wird dann inkl. der vorherigen Restirktionen durch Zufall bestimmt.
	 * 
	 * 
	 * 
	 * Eventuell noch:
	 * - auf den Gegner mit den wenigsten Schiffen ballern - nicht bei nur noch
	 * 2 Schiffen
	 * 
	 * Testen auf lineare Plazierungs-Strategie der Gegner?
	 * - mittig der Intervalle ballern - +1 und -1 
	 *  * 
	 * 
	 */
}
